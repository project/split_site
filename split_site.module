<?php
/*
 *   Module to share menu systems across a site that has been split to enhance performance.  See README.txt 
 */
  
/**
 * Implementation of hook_help().
 */
function split_site_help($section) {
  switch ($section) {
    case 'admin/help#split_site':
      $output = '<p><b>'. t('This module is for experienced users only.  It requires manual configuration of the database and ability to use subdomains.') .'</b></p>';
      $output .= '<p>'. t('The split site module optimizes a Drupal site that uses a large number of modules (80 or more) by splitting into a number of smaller sites each using a subdomain.  Unlike other multi-site options which use different databases or which split the sites into separate tables, this option minimizes the number of tables that are shared.  The tables which are split out are as follows:') .'</p>';
	  $output .= '<ul><li>menu</li><li>all caches</li><li>system</li><li>blocks</li><li>blocks_roles</li><li>boxes</li><li>variable</li><li>node_access</li></ul>';
      $output .= '<p>'. t('The greatest amount of performance optimization is achieved by splitting out system, blocks and menu.  Each module you enable adds a script processing load because each is checked for Drupal hooks and some load css or other items with the page. The system table stores information on what modules are activated on a site and then menus, blocks and node_access rules are created dynamically from those modules, so these four need to be kept together.  When you configure this modules the extra tables are created and you can also split out other tables as desired, such as the node tables.  What tables you want to split out depends on what level of optimzation or customization you want to achieve.') .'</p>';
      $output .= '<h3>'. t('Installation') .'</h3>';
      $output .= '<p>'. t('Split your site into sub-sites, create the subdomains, the directories in /sites and configure settings.php in each correctly.') .'</p>';
      $output .= '<p>'. t('Activate module in the site which you will use as the master site. DO NOT activate this module in the subsites.') .'</p>';
      $output .= '<p>'. t('In admin/settings/split_site select the menu systems that you want to share across the sub-sites and also the names of the prefixes to the tables that belong to the sub-sites. This module attempts to recreate the selected menus in the master site across each of the sub-sites.  If you intend a menu link in a shared menu to go to a subdomain then use \'http://subdomain.mydomain.com\' so that the user goes to the intended destination.  If you do not then the module inserts the \'http://\' address of the master domain which ensures that all links on the shared menus that are not assigned to a subdomain go to the master domain.').'</p>';
$output .= '<p>'. t('	  This system works provided the menu systems are activated through the blocks system and not processed directly through the page.tpl.php template.  If you have a complex menu system then this module may not work for you.') .'</p>';
      return $output;	
  }	
}
function split_site_menu($may_cache) {
  global $user;
  if ($may_cache) {
   $items[] = array(
	  'path' => 'admin/settings/split_site',
      'title' => t('Site Splitting'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('split_site_settings_form'),
      'access' => user_access('administer site configuration'),
      'type' => MENU_NORMAL_ITEM
 	);
	split_site_process_shared_menus();
  }
  return $items;
}

function split_site_settings_form() {
  // settings for default and content type
  $result = db_query("SELECT mid, title FROM {menu} WHERE pid=0 ORDER BY mid");  
  $options = array( 1 => 'navigation');  // navigation menu has pid=1 but doesn't appear in the db
  while($row = db_fetch_object($result)) {
    $options[$row->mid] = $row->title;
  }
  $form['split_site_menus'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Shared Site Menus'),
    '#default_value' => variable_get('split_site_menus', ''),
	'#options' 		 => $options,
    '#description'   => t("Select the menu systems you wish to share.  These are created only in the Master domain, and then copied to the subdomains to keep them uniform across the site.  Note that if you change a menu name then you will have to reactivate any blocks in the subdomains."),
  );
  $form['split_site_tables'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Shared Site Tables'),
    '#default_value' => variable_get('split_site_tables', ''),
    '#description'   => t("Enter the names of extra tables that are not shared, separated by a comma.  Don't use any prefixes here. The following tables are created automatically for each prefix you enter in the field below: <br /> system,blocks,blocks_roles,boxes,node_access,variable,menu,cache,cache_menu,cache_page,cache_views,cache_content. <br />
	When you submit this form the required and requested tables will be created (if they don't already exist). If you add a table here you have to also add it to the settings.php form in the relevant subdomain so that the script knows where to look."),
  );
  $form['split_site_prefixes'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Shared Site Table Prefixes'),
    '#default_value' => variable_get('split_site_prefixes', ''),
    '#description'   => t("Enter the prefixes for the tables for each of the subdomains used in the site, separated by a comma.  Do not include the underscore.  Note: it is assumed that an underscore is used as per Drupal suggested practice."),
  );
 return split_site_system_settings_form($form);
}

/**
 * Add default buttons to a form and set its prefix
 */
function split_site_system_settings_form($form) {
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#base'] = 'split_site_system_settings_form';
  return $form;
}
/**
 * Execute the split site system_settings_form.
 *
 * If you want node type configure style handling of your checkboxes,
 * add an array_filter value to your form.
 *
 */
function split_site_system_settings_form_submit($form_id, $form_values) {
  $op = isset($form_values['op']) ? $form_values['op'] : '';

  // Exclude unnecessary elements.
  unset($form_values['submit'], $form_values['reset'], $form_values['form_id'], $form_values['op'], $form_values['form_token']);

  foreach ($form_values as $key => $value) {
    if ($op == t('Reset to defaults')) {
      variable_del($key);
    }
    else {
      if (is_array($value) && isset($form_values['array_filter'])) {
        $value = array_keys(array_filter($value));
      }
      variable_set($key, $value);
    }
  }
  $prefixes = explode(",", $form_values['split_site_prefixes']);
  if($form_values['split_site_tables']!='') $tables = 'system,blocks,blocks_roles,boxes,node_access,variable,menu,cache,cache_menu,cache_page,cache_views,cache_content,'.$form_values['split_site_tables'];
  else $tables = 'system,blocks,blocks_roles,boxes,node_access,variable,menu,cache,cache_menu,cache_page,cache_views,cache_content';
  $tables = explode(",", $tables);
  $modules = array('block', 'system', 'user', 'filter', 'watchdog', 'menu', 'node');
  
  //  Check for existence of tables and create if not present.
  foreach($tables as $table) {
    foreach($prefixes as $prefix) {
      if(!db_table_exists($prefix.'_'.$table)) {
	    // we need to enable core modules in the system table for the subdomain to work
	    if($table=="system") db_query("CREATE TABLE ".$prefix."_".$table." SELECT * FROM {".$table."} WHERE name IN('block', 'system', 'user', 'filter', 'watchdog', 'menu', 'node', 'garland')");
	    else db_query("CREATE TABLE ".$prefix."_".$table." LIKE {".$table."}");
		if($table=="menu") db_query("ALTER  TABLE ".$prefix."_".$table." ADD share TINYINT( 1 ) NOT NULL DEFAULT '0'");
		drupal_set_message('Table '.$prefix.'_'.$table.' created');
	  }
	  else drupal_set_message('Table '.$prefix.'_'.$table.' already exists');
	}
  }
  if ($op == t('Reset to defaults')) {
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    drupal_set_message(t('The configuration options have been saved.'));
  }

  menu_rebuild();
}

function split_site_process_shared_menus() {
  global $alias, $blocks;
  $arr = variable_get('split_site_menus', array());
  $menus = array();
  if(is_array($arr)) {
    foreach($arr as $a) {
      if($a>0) $menus[] = $a;
    }
  }
  // only process if we have menus
  if(is_array($menus)) {
    $prefixes = explode(",", variable_get('split_site_prefixes', ''));
    if(!empty($prefixes)) {
      foreach($prefixes as $prefix) {
		$items = array();
		$data = array();
		foreach($menus as $mid) {
		  $title = db_result(db_query("SELECT title FROM {menu} WHERE mid=%d", $mid));
		  $data[$title] = $mid;
		  if($mid>0) $items = array_merge($items, array($mid), split_site_menu_tree($mid));
		}
		// find any menu blocks
		if(db_table_exists($prefix.'_blocks') && db_table_exists($prefix.'_menu')) {
		  $result = db_query("SELECT b.delta, m.title FROM ".$prefix."_blocks b INNER JOIN ".$prefix."_menu m ON b.delta=m.mid WHERE b.module='menu'");
		  $blocks[$prefix] = array();
          while($row = db_fetch_object($result)) {
		    $blocks[$prefix][$row->delta] = $row->title;
		  }
          $result = db_query("SELECT mid FROM ".$prefix."_menu WHERE share=1 ORDER BY mid");
          while($row = db_fetch_object($result)) {
            if(!empty($items)) split_site_save_menu_item(array_shift($items), $prefix, $blocks, $row->mid);
	        // if we have less items to propagate than there are menu mids allocated
		  
            else db_query("DELETE FROM ".$prefix."_menu WHERE mid=%d", $row->mid);
          }
          // process any remain items
          foreach($items as $item) {
            split_site_save_menu_item($item, $prefix, $blocks);   
          }
          // now alter pids
          $result = db_query("SELECT mid, pid FROM ".$prefix."_menu WHERE share=1");
          while($row = db_fetch_object($result)) {
            db_query("UPDATE ".$prefix."_menu SET pid=%d WHERE mid=%d", $alias[$prefix][$row->pid], $row->mid);
          }
          // look  for nice menu menus
  
          $nice = FALSE;
          $c = 1;
          while($nice == TRUE) {
            if(variable_get('nice_menus_menu_'.$c, 0)) {
	          if(isset($data[variable_get('nice_menu_name_'.$c, '')])) variable_set('nice_menus_menu_'.$c, $data[variable_get('nice_menu_name_'.$c, '')]);
	        }
	        else $nice = FALSE;
	        $c++;
          }
		  // clear cache so menu rebuilds correctly
		  db_query("TRUNCATE TABLE ".$prefix."_cache_menu");
		}
      }
    }
  }	
}

function split_site_menu_tree($pid = 1) {
  $menu = menu_get_menu();
  $result = db_query("SELECT mid FROM {menu} WHERE pid=%d", $pid);
  while($menu = db_fetch_object($result)) {
 	  $count = db_result(db_query("SELECT COUNT(*) FROM {menu} WHERE pid=%d", $menu->mid));
	  if($count>0) $tree = array_merge($tree, split_site_menu_tree($menu->mid));
      $tree[$menu->mid] = $menu->mid;
  }
  if(!empty($tree)) return $tree;
  else return array();
}
// this should work but for some reason does not
function split_site_menu_tree_old($pid = 1) {
  $menu = menu_get_menu();
  if (isset($menu['visible'][$pid]) && $menu['visible'][$pid]['children']) {
    foreach ($menu['visible'][$pid]['children'] as $mid) {
      $type = isset($menu['visible'][$mid]['type']) ? $menu['visible'][$mid]['type'] : NULL;
      $children = isset($menu['visible'][$mid]['children']) ? $menu['visible'][$mid]['children'] : NULL;
	  
	  if(count($children)>0) {
	    if(is_array($tree)) $tree = array_merge($tree, split_site_menu_tree($mid));
		else $tree = split_site_menu_tree($mid);
	  }
      $tree[$mid] = $mid;
    }
  }
  if(!empty($tree)) return $tree;
  else return array();
}
/**
 * Save a menu item to the database.
 *
 * @param $item
 *   The menu item to be saved. This is passed by reference, so that the newly
 *   generated $item['mid'] can be accessed after an insert takes place.
 *
 * @return $status
 *   The operation that was performed in saving. Either SAVED_NEW (if a new
 *   menu item was created), or SAVED_UPDATED (if an existing menu item was
 *   updated).
 */
function split_site_save_menu_item($core_mid, $prefix, $blocks, $mid=0) {
  global $alias, $blocks;
    $item = db_fetch_array(db_query("SELECT * FROM {menu} WHERE mid=%d", $core_mid));
    if ($mid>0) {
	  // If the path does not include an http:// then direct to the master domain as per single signon settings
	  if(!strpos($item['path'], 'http://')) $item['path'] = variable_get('singlesignon_master_url', '').'/'.$item['path'];
      db_query("UPDATE ".$prefix."_menu SET pid = %d, path = '%s', title = '%s', description = '%s', weight = %d, type = %d, share=1 WHERE mid = %d", $item['pid'], $item['path'], $item['title'], $item['description'], $item['weight'], $item['type'], $mid);
      $alias[$prefix][$core_mid] = $mid;
	  // if the menu item is a menu system then update block info using the menu name as an identifier.  If the menu name is changed then this doesn't work
	  if(in_array($item['title'], $blocks[$prefix]) && $item['pid']==0) {
	    foreach($blocks[$prefix] as $key => $title) {
		  if($title == $item['title']) {
            db_query("UPDATE ".$prefix."_blocks SET delta=%d WHERE module='menu' AND delta=%d", $mid, $key );	
		  }
		}
	  }
      return SAVED_UPDATED;
    }
    else {
      $item['mid'] = db_next_id($prefix.'_menu_mid');
      // Check explicitly for mid <= 2. If the database was improperly prefixed,
      // this would cause a nasty infinite loop or duplicate mid errors.
      // TODO: have automatic prefixing through an installer to prevent this.
      while ($item['mid'] <= 2) {
        $item['mid'] = db_next_id($prefix.'_menu_mid');
      }
	  // If the path does not include an http:// then direct to the master domain as per single signon settings
	  if(!strpos($item['path'], 'http://')) $item['path'] = variable_get('singlesignon_master_url', '').'/'.$item['path'];
      db_query("INSERT INTO ".$prefix."_menu (mid, pid, path, title, description, weight, type, share) VALUES (%d, %d, '%s', '%s', '%s', %d, %d, %d)", $item['mid'], $item['pid'], $item['path'], $item['title'], $item['description'], $item['weight'], $item['type'], 1);
      $alias[$prefix][$core_mid] = $item['mid'];
      return SAVED_NEW;
	}
}