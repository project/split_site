split_site README

This module assumes the following:

You have a good understanding of Drupal
You are able to set up subdomains
You understand and can edit settings.php
You have a site of 80 or more modules and want to optimize it for performance
You can successfully split up your site into subdomains each using about 40 modules or less

Installation:
Requires single signon module
Install the module into the master domain only and set up your site menu system in the master domain only. DO NOT INSTALL IN ANY SUBDOMAINS. This module propagates selected menu systems (eg primary and secondary links) across each subdomain (admin/settings/split_site).  You will have to edit menu items and insert http://subdomain.site.com in order to make the menu items direct users to the correct subdomains. The module also attempts to find any blocks currently holding these menus in order to maintain their status on the site.

Instructions can be found at 'admin/help#split_site'

Each module installed into each site has to be configured for that site.

The following is a sample array of prefixes (settings.php) for the minimum configuration required to maket this module work

$db_prefix =  array(
   'default'    => '',
      'blocks'   => 'jobs_',
      'blocks_roles'   => 'jobs_',
      'boxes'   => 'jobs_',
      'system'  => 'jobs_',
      'menu'  => 'jobs_',
      'variable'  => 'jobs_',
      'cache_views'  => 'jobs_',
      'cache'  => 'jobs_',
      'cache_page'  => 'jobs_',
      'cache_content'  => 'jobs_',
      'cache_menu'  => 'jobs_',
      'node_access'  => 'jobs_',
	);
